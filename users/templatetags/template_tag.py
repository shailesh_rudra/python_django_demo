from django import template
from django.conf import settings

from users.models import User

register = template.Library()


# @register.filter(name='count')
# @stringfilter
# def count(value, args):
#     if str(args) == "user":
#         User = get_user_model()
#         return User.objects.filter(is_deleted=False).count() - 1
@register.filter(name='model_count')
def model_count(value, args):
    if str(args) == "users":
        return User.objects.filter(is_deleted=False).count() -1
    return 0


@register.filter(name='pass_dict')
def pass_dict(value, args):
    try:
        # print value[args - 1]
        return value[args - 1]
    except:
        return ""


@register.filter(name='remove_last_value')
def remove_last_value(value):
    print(value.split("/")[-2])
    try:
        if value.split("/")[-2] != "change":
            val = "/".join(value.split("/")[:-2])
        else:
            val = "/".join(value.split("/")[:-3])
    except:
        val = "/"
    return val


@register.filter(name='get_param_value')
def get_param_value(value, args):
    try:
        # print(value, args)
        # print value[str(args)]
        return value[str(args)]
    except:
        return ""


@register.filter(name='get')
def get(d, k):
    return d.get(k, None)


@register.filter(name='definitions')
def definitions(value, args):
    print(args)
    if str(args) == "RECAPTCHA_ENABLED":
        return 1 if settings.RECAPTCHA_ENABLED else 0
    if str(args) == "RECAPTCHA_SITE_KEY":
        return settings.RECAPTCHA_SITE_KEY or ""
    elif str(args) == "app_name":
        return settings.APP_NAME or "Airssist"
    elif str(args) == "main_logo":
        return settings.STATIC_URL + "images/logo-on-dark.jpg"
    elif str(args) == "no_image":
        return settings.STATIC_URL + "images/logo-on-dark.jpg"
    else:
        return True


@register.inclusion_tag('admin/import_file.html', takes_context=True)
def admin_import(context):
    """
    Track the number of times the action field has been rendered on the page,
    so we know which value to use.
    """
    context['action_index'] = context.get('action_index', -1) + 1
    return context
