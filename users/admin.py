# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from base.admin import AbstractModelAdmin
from users.forms import UserAddForm
from users.models import User


@admin.register(User)
class UserAdmin(AbstractModelAdmin):
    list_display = ['first_name', 'last_name', 'action_column']
    form = UserAddForm

    def get_queryset(self, request):
        qs = super(UserAdmin, self).get_queryset(request)
        qs = qs.filter(is_staff=False, is_superuser=False, is_deleted=False).exclude(id=request.user.id).order_by("-id")
        if request.user.is_superuser:
            return qs
        return qs.filter(i_by=request.user.id)
