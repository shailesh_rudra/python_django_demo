# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models

from base.models import BaseModel


class User(AbstractUser, BaseModel):
    country_code = models.CharField(max_length=5, blank=True, null=True, default="")
    contact_number = models.CharField(max_length=30, blank=True, null=True, default="")

    def __str__(self):
        return "{0}".format(self.email)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

        permissions = (
            ("view_{model_name}".format(model_name="user"), "Can view {0}".format("user")),
            ("list_{model_name}".format(model_name="user"), "Can list {0}".format("user")),
        )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.username = self.email
        u = super(User, self).save(force_insert=False, force_update=False, using=None,
                                   update_fields=None)
        return u  # save needs to return a `User` object, remember!
