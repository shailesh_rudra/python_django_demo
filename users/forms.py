from django import forms
from django.core.exceptions import ValidationError
from django.forms.widgets import TextInput

from users.models import User


class ConfirmPasswordForm(forms.ModelForm):
    confirm_password = forms.CharField(required=True, widget=TextInput(attrs={'type': 'password'}))

    def clean(self):
        cleaned_data = super(ConfirmPasswordForm, self).clean()
        if cleaned_data.get("password") != cleaned_data.get("confirm_password"):
            cleaned_data["password"] = ""
            cleaned_data["confirm_password"] = ""
            raise ValidationError({'confirm_password': ["Password and confirm password not same", ]})
        return cleaned_data


class EmailCheckForm(forms.Form):
    def clean_email(self):
        email = self.cleaned_data.get('email')
        print(User.objects.filter(username=email).count())
        if self.instance.id:
            user_email_count = User.objects.filter(username=email).exclude(id=self.instance.id).count()
        else:
            user_email_count = User.objects.filter(username=email).count()
        print(user_email_count)
        if user_email_count > 0:
            raise ValidationError("Email already taken by another user")
        return email


class UserEditForm(forms.ModelForm, EmailCheckForm):
    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        initial = kwargs.pop('initial', {})
        edit = False
        if self.instance and self.instance.pk:
            # print("Edit")
            edit = True
        for key in self.fields:
            if key in ['first_name', 'last_name', 'email', 'password']:
                self.fields[key].required = True
                pass
            if edit:
                pass

            self.fields[key].widget.attrs['class'] = 'form-control'
        kwargs['initial'] = initial
        super(UserEditForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(UserEditForm, self).clean()
        cleaned_data["is_staff"] = True
        print("cleaned_data: ", cleaned_data)
        return cleaned_data

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']


class UserAddForm(UserEditForm, ConfirmPasswordForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name','email', 'password', ]
