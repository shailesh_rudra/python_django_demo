$(document).on('click','body .inline-deletelink',function(){
    console.log("this is my best");
    $(this).closest('.form-row').remove();
    //  $(this) = your current element that clicked.
    // additional code
});
var input = document.getElementById('id_confirm_password');
if (input){
    input.value = '';
}

var input = document.getElementById('id_password');
if (input){
    input.value = '';
}

var input = document.getElementById('id_corporateadmin_set-0-password');
if (input){
    input.value = '';
}


// Custom functions

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
};

function minmax(value, min, max)
{
    if(parseInt(value) < min || isNaN(parseInt(value)))
        return min;
    else if(parseInt(value) > max)
        return max;
    else return value;
};


// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
function isFloat(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function preciseFloat2place(evt, element){
    var num = parseFloat($(element).val());
    var cleanNum = num.toFixed(2);
    $(element).val(cleanNum);
    if(num/cleanNum < 1){
        $('#error').text('Please enter only 2 decimal places, we have truncated extra points');
        }
    }