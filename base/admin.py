import json
import sys

from django import forms
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import SimpleListFilter
from django.core import urlresolvers
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, HttpResponse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


def make_active(modeladmin, request, queryset):
    if queryset.update(is_active=True):
        messages.add_message(request, messages.SUCCESS, mark_safe('Data successfully activated.'))
    else:
        messages.add_message(request, messages.SUCCESS, mark_safe('Something went wrong. Please try again later.'))


def make_inactive(modeladmin, request, queryset):
    if queryset.update(is_active=False):
        messages.add_message(request, messages.SUCCESS, mark_safe('Data successfully de-activated.'))
    else:
        messages.add_message(request, messages.SUCCESS, mark_safe('Something went wrong. Please try again later.'))


make_active.short_description = "Activate selected"
make_inactive.short_description = "Deactivate selected"


class StatusFilter(SimpleListFilter):
    title = _('Status')

    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return (
            (True, _('Active')),
            (False, _('Inactive')),
            (None, _('All')),
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        # print(self.value())
        if not self.value():
            return queryset.filter()
        # elif self.value() == "all":
        #     return queryset.filter()
        else:
            return queryset.filter(is_active=self.value())


class AbstractModelAdmin(admin.ModelAdmin):
    list_per_page = 5
    # list_filter = ('is_active',)
    list_filter = [StatusFilter]
    list_display_links = None
    # Import functionality
    import_function = False
    import_form = ""

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'password':
            kwargs['widget'] = forms.PasswordInput()
        return super(AbstractModelAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    # http://stackoverflow.com/questions/17584532/how-to-create-a-custom-date-time-widget-for-the-django-admin
    formfield_overrides = {
        # models.DateField: {'widget': DateWidgets(format='%m/%d/%Y')},
        # models.TimeField: {'widget': TimeWidgets()},
        # models.DateTimeField: {'widget': DateTimeWidgets()},
    }

    def get_queryset(self, request):
        qs = super(AbstractModelAdmin, self).get_queryset(request)
        qs = qs.filter().order_by("-id")
        if request.user.is_superuser:
            return qs
        return qs.filter(i_by=request.user.id)

    def get_actions(self, request):
        actions = super(AbstractModelAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def delete_selected(self, request, queryset):
        # delete related
        for obj in queryset:
            links = [rel.get_accessor_name() for rel in obj._meta.related_objects]
            for link in links:
                objects = getattr(obj, link).all()
                for object in objects:
                    try:
                        object.is_deleted = True
                        object.is_active = False
                        object.save()
                    except Exception as E:
                        print (E)

        updated = queryset.update(is_deleted=True, is_active=False)
        if updated == 1:
            msg = '1 record has been deleted successfully'.format(updated)
        else:
            msg = '{0} records deleted successfully'.format(updated)
        messages.success(request, msg)

    actions = ()  # actions = (delete_selected,)

    def get_urls(self):
        urls = super(AbstractModelAdmin, self).get_urls()
        my_urls = [
            url(r'^view/(?P<id>[0-9]+)/$', self.view_details,
                name="{0}_{1}_{2}".format(self.model._meta.app_label, self.model._meta.model_name, 'view')),
            # url(r'^delete/(?P<id>[0-9]+)/$', self.delete_record,
            #     name="{0}_{1}_{2}".format(self.model._meta.app_label, self.model._meta.model_name, 'delete')),
            url(r'^delete/$', self.delete,
                name="{0}_{1}_{2}".format(self.model._meta.app_label, self.model._meta.model_name, 'delete')),
            # url(r'^change_status/$', self.change_status_record,
            #    name="{0}_{1}_{2}".format(self.model._meta.app_label, self.model._meta.model_name, 'change_status')),
            url(r'^change_status/$', self.change_status,
                name="{0}_{1}_{2}".format(self.model._meta.app_label, self.model._meta.model_name, 'changestatus')),
        ]
        return my_urls + urls

    def view_details(self, request, id):
        if not request.user.has_perm('{0}.view_{1}'.format(self.model._meta.app_label, self.model._meta.model_name)):
            raise PermissionDenied
        obj = self.model.objects.filter(id=id).first()
        context = {
            "obj": obj,
            "obj_title": self.model._meta.model_name
        }
        return render(request, self.view_detail_page, context)

    def delete(self, request):
        if not request.user.has_perm('{0}.change_{1}'.format(self.model._meta.app_label, self.model._meta.model_name)):
            raise PermissionDenied
        objectid = request.POST.get('data-id')
        obj = self.model.objects.filter(id=objectid).first()
        obj.is_active = False
        obj.is_deleted = True
        obj.save()
        try:
            links = [rel.get_accessor_name() for rel in obj._meta.related_objects]
            # print(links)
            for link in links:
                # print(link, "$$")
                objects = getattr(obj, link).all()
                for object in objects:
                    # print("##", object)
                    try:
                        object.is_deleted = True
                        object.is_active = False
                        object.save()
                    except Exception as E:
                        print (E)
        except:
            pass
            print(sys.exc_info())
        messages.success(request, '{0} is deleted successfully.'.format(self.model._meta.app_label))
        return HttpResponse("success")

    def change_status(self, request):
        if not request.user.has_perm('{0}.change_{1}'.format(self.model._meta.app_label, self.model._meta.model_name)):
            raise PermissionDenied
        objectid = request.POST.get('data-id')
        obj = self.model.objects.filter(id=objectid).first()

        if obj.is_active:
            obj.is_active = False
        else:
            obj.is_active = True
        obj.save()

        return_arr = {
            'active': "yes" if obj.is_active else "no", 'data-slug-id': obj.id,
            'data-url': urlresolvers.reverse(
                "admin:%s_%s_changestatus" % (self.model._meta.app_label, self.model._meta.model_name))}
        return HttpResponse(json.dumps(return_arr))

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['import_function'] = self.import_function
        extra_context['import_sample_file'] = "{0}{1}".format(settings.STATIC_URL, self.import_form)
        extra_context['title'] = '{0} List'.format(str(self.model._meta.verbose_name).title())
        extra_context['link'] = 'add'
        extra_context['site_title'] = 'Peerbits'
        extra_context['addtitle'] = 'Add new {0}'.format(self.model._meta.verbose_name)
        return super(AbstractModelAdmin, self).changelist_view(request, extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        print("Save model")
        if obj.pk is None:
            # For add view
            print("Add")
            obj.i_by = request.user.id
        print("Edit")
        # For Edit view
        obj.u_by = request.user.id
        super(AbstractModelAdmin, self).save_model(request, obj, form, change)

    class Media:
        js = ('admin/appjs/common.js',)

    class Meta:
        abstract = True

        # def delete_record(self, request, id):
        #     if not request.user.has_perm('{0}.delete_{1}'.format(self.model._meta.app_label, self.model._meta.model_name)):
        #         raise PermissionDenied
        #     obj = self.model.objects.filter(id=id).first()
        #     try:
        #         # delete related first
        #         links = [rel.get_accessor_name() for rel in obj._meta.related_objects]
        #         for link in links:
        #             objects = getattr(obj, link).all()
        #             for object in objects:
        #                 try:
        #                     object.is_deleted = True
        #                     object.is_active = False
        #                     object.save()
        #                 except Exception as E:
        #                     print (E)
        #         obj.is_active = False
        #         obj.is_deleted = True
        #         obj.save()
        #     except Exception as E:
        #         print ("===>>>>", E)
        #     return HttpResponseRedirect(reverse("admin:{0}_{1}_changelist".format(
        #         self.model._meta.app_label, self.model._meta.model_name)))

        # def change_status_record(self, request):
        #    if not request.user.has_perm('{0}.change_{1}'.format(self.model._meta.app_label, self.model._meta.model_name)):
        #        raise PermissionDenied
        #    id = request.GET.get('id')
        #    obj = self.model.objects.filter(id=id).first()
        #    try:
        #        if obj.is_active:
        #            obj.is_active = False
        #        else:
        #            obj.is_active = True
        #        obj.save()
        #    except Exception as E:
        #        print (E)
        #    return HttpResponseRedirect(reverse("admin:{0}_{1}_changelist".format(
        #        self.model._meta.app_label, self.model._meta.model_name)))
