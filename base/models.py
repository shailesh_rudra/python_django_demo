from __future__ import unicode_literals

from django.core import urlresolvers
from django.db import models


class DeletedManager(models.Manager):
    def get_queryset(self):
        return super(DeletedManager, self).get_queryset().filter(is_deleted=False)


class BaseModel(models.Model):
    """
    An abstract base class model to add basic db fields
    """
    objects = DeletedManager()
    add_common_fields = True
    show_edit_icon = True
    show_view_icon = False
    show_delete_icon = True

    if add_common_fields:
        i_by = models.IntegerField(blank=True, null=True)
        u_by = models.IntegerField(blank=True, null=True)
        is_active = models.BooleanField(blank=True, default=True, verbose_name="Status")
        is_deleted = models.BooleanField(blank=True, default=False)
        created = models.DateTimeField(auto_now_add=True)
        modified = models.DateTimeField(auto_now=True)

    def action_column(self):
        links_start = """
        <div class="btn-group">
                                <a href="#" title="" class="btn btn-default btn-sm btn-icon-only dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" role="menu">
        """
        links_end = """
        </ul>
                                </div>
        """
        main_link = ""

        if self.show_edit_icon:
            url = urlresolvers.reverse("admin:%s_%s_change" % (self._meta.app_label, self._meta.model_name),
                                       args=(self.pk,))
            # edit_link = """
            #             <a href='""" + edit_url + """' title='Edit Details' class="btn btn-default btn-sm btn-icon-only">
            #                 <i class="fa fa-pencil"></i>
            #             </a>
            #         """
            main_link += '<li><a href="{url}">Edit</a></li>'.format(url=url)

        if self.show_view_icon:
            url = urlresolvers.reverse("admin:%s_%s_view" % (self._meta.app_label, self._meta.model_name),
                                       args=(self.pk,))
            # view_link = """<a href='""" + view_url + """' title='View Details' class='btn btn-default btn-sm btn-icon-only'>
            #             <i class='fa fa-info'></i>
            #         </a>"""
            main_link += '<li><a href="{url}">View</a></li>'.format(url=url)

        if self.show_delete_icon:
            url = urlresolvers.reverse("admin:%s_%s_delete" % (self._meta.app_label, self._meta.model_name))
            # delete_link = """
            #            <a id="delete-status" data-slug-id='""" + str(
            #     self.pk) + """' title='Delete Details'  data-url='""" + delete_link + """' href='javascript:void(0);' class="btn btn-default btn-sm btn-icon-only">
            #                    <i class="fa fa-trash"></i>
            #            </a>
            #          """
            main_link += '<li> <a id="delete-status" data-slug-id="{pk}" data-url="{url}"  href="javascript:void(0);">Delete</a></li>'.format(
                url=url,
                pk=self.pk)

        return "".join([links_start, main_link, links_end])

    action_column.allow_tags = True
    action_column.short_description = "actions"

    def status_column(self):
        data_url = urlresolvers.reverse("admin:%s_%s_changestatus" % (self._meta.app_label, self._meta.model_name))
        inactive = "<a data-slug-id='" + str(
            self.pk) + "' data-url='" + data_url + "' href='javascript:void(0);' id='change-status' class='label label-danger'>Inactive</a>"
        active = "<a data-slug-id='" + str(
            self.pk) + "' data-url='" + data_url + "' href='javascript:void(0);' id='change-status' class='label label-success'>Active</a>"

        if self.is_active:
            return active
        else:
            return inactive

    status_column.allow_tags = True
    status_column.short_description = "Status"

    class Meta:
        abstract = True
        # permissions = (
        #   ("view_user".format(model_name), "Can view {0}".format(model_name)),
        # )
