# python-django-demo-app

python-django-demo-app is a **minimal** sample application to start developing your web project immediately on [Django framework](https://www.djangoproject.com/).
This app contains custom user module with add/edit/delete functionality

## Installation

### 1. virtualenv
$ virtualenv venv2.7

### 2. Download
Now, you need the **python-django-demo-app** project files in your workspace:
$ git clone https://bitbucket.org/peerbits-admin/python_django_demo.git

### 3. Requirements
Right there, you will find the *requirements.txt* file that has all the great debugging tools, django helpers and some other cool stuff. To install them, simply type:
`$ source venv2.7/bin/activate`
`$ pip install -r requirements.txt`

### 4. DB Migration
If you want to continue to sqlite db then skip this step.
This repository contains squlite db as demo db.

To check db in Mysql/PGsql need to update settings file. below is mysql setting example.
```
 DATABASES = {
    'default': {
        'ENGINE': "django.db.backends.mysql",
        'NAME': "["DATABASES"]["NAME"]",
        'HOST': "localhost",
        'PORT': 3306,
        'USER': "["DATABASES"]["USER"]",
        'PASSWORD': "["DATABASES"]["PASSWORD"]",
    }
}
```

`$ python manege.py makemigrations`
`$ python manege.py migrate`


### 5. Run project
$ python manage.py runserver 0.0.0.0:8000


Sqlite db is attached with this repository.
admin user credentials:
username: admin
email: admin@peerbits.com
password: admin
