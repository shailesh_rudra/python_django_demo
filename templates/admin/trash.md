**Home page breadcrumbs**:
'''
{% if not request.GET|get_param_value:"_popup" %}
    {% if request.path == '/admin/'%}
        {% block breadcrumbs %}
            <section class="content-header">
                <h1>
                  Dashboard{{ request.path }}
                </h1>
                <ol class="breadcrumb">
                  <li><a href="{% url 'admin:index' %}"><i class="fa fa-dashboard"></i> {% trans 'Home' %} </a></li>
                  <li class="active">{% if title %} {{ title }} {% endif %}</li>
                </ol>
            </section>
        {% endblock %}
    {% endif %}
{% endif %}
'''

